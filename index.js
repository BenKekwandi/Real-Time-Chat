const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const app = express()
const passport = require('passport')
const session = require('express-session');
const routes = require('./routes/router')
const authRoutes = require('./auth')
const port = 8000 

app.use(session({
    secret: 'ewoinewiof6rf4rf4vrv8528e627e((*@&^^%%$52254sdhdf000*%@rfVH@%$@RWFGHUI@O@553ex3',
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.use(passport.initialize());
app.use(passport.session());

app.use('/api',routes)

app.use('/auth',authRoutes)

app.get('/',(req,res)=>{
    res.render('live');
})

appRouter = require('./app')

app.use('/app',appRouter)

const dbString = 'mongodb://localhost:27017/chat'

mongoose.connect(dbString, {
    useNewUrlParser: false,
    useUnifiedTopology: false
  }).then(() => {
    console.log('Connected to MongoDB');
    app.listen(port,()=>{
        console.log(`Server is running at port ${port}`)
    })

  }).catch((error) => {
    console.error('Error connecting to MongoDB:', error);
  });
  