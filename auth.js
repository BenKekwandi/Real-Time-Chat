const router = require('express').Router();
const jwt = require('jsonwebtoken')


const secretKey = 'ewoinewiof6rf4rf4vrv8528e627e((*@&^^%%$52254sdhdf000*%@rfVH@%$@RWFGHUI@O@553ex3'

const authenticateUser = (req, res, next) => {
    const token = req.headers.authorization;
    if (!token) {
      return res.status(401).json({ message: 'Authentication failed: No token provided' });
    }
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) {
        return res.status(401).json({ message: 'Authentication failed: Invalid token' });
      }
      req.user = decoded;
      next();
    });
};

router.post('login', (req,res)=>{
    const { username, password } = req.body;

    User.findOne({ username: username }, (err, user) => {
        if (err) { return done(err); }
        if (!user) {
          return done(null, false, { message: 'Incorrect username.' });
        }
        if (!user.validatePassword(password)) {
          return done(null, false, { message: 'Incorrect password.' });
        }

        const token = jwt.sign({ id: user.id, username: user.username }, secretKey);
        res.json({ message: 'Login successful', token });
        return done(null, user);
    });
  
})

router.post('logout', (req,res)=>{
    
})
router.post('register', (req,res)=>{
    
})

module.exports = router;