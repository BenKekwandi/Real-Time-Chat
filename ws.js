const WebSocket = require('ws');
const User = require('./models/user');

const wss = new WebSocket.Server({ port: 9090 });

// Fetch user data from the database
async function fetchUserData() {

    try {
        fetch('http://127.0.0.1:8000/api/user', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response)=>{
            console.log(response.json())
        })
    }
    catch(error){
        console.log('Error fetching user data:', error);
    }

    // try {
    //     const users = await User.find().lean().maxTimeMS(30000); // Increase timeout to 30 seconds
    //     return { users };
    // } catch (error) {
    //     console.error('Error fetching user data:', error);
    //     return { error: 'Error fetching user data' };
    // }
}


// Broadcast user data to all connected clients
async function broadcastUserData() {
    try {
        const data = await fetchUserData();
        wss.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN) {
                client.send(JSON.stringify(data));
            }
        });
    } catch (error) {
        console.error('Error broadcasting user data:', error);
    }
}

// Send data updates every second
setInterval(broadcastUserData, 1000);

console.log('WebSocket server listening on port 9090');
