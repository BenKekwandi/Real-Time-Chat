const router = require('express').Router();
const User = require('../models/user')


router.get('', async (req, res) => {
    try {
        const users = await User.find();
        res.status(200).json(users);
    } catch (error) {
        console.error('Error fetching users:', error);
        res.status(500).json({ error: 'Internal Server Error' }); // Handle errors gracefully
    }
});
router.get('',(req,res)=>{

})

router.post('',(req,res)=>{
    
})
router.put('',(req,res)=>{
    
})
router.delete('',(req,res)=>{
    
})

module.exports = router;