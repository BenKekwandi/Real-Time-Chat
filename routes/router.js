const router = require('express').Router();

const user = require('./user');
const message = require('./message');
const chat = require('./chat');


router.use('/user',user)
router.use('/chat',chat)
router.use('/message',message)

module.exports = router;
