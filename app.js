const route = require('express').Router()
const passport = require('passport');
const bcrypt = require('bcrypt')
const LocalStrategy = require('passport-local').Strategy;

const User = require('./models/user')

function isAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    res.redirect('/app/auth-login')
}
  

passport.use(new LocalStrategy(
    async (username, password, done) => {
      try {
        const user = await User.findOne({ username : username });
        if (!user || !user.verifyPassword(password)) {
          return done(null, false, { message: 'Invalid username or password' });
        }
        return done(null, user);
      } catch (err) {
        return done(err);
      }
    }
));


passport.serializeUser((user, done) => {
    done(null, user.id);
});
  
passport.deserializeUser(async (id, done) => {
    try {
        const user = await User.findById(id);
        done(null, user);
    } catch (err) {
        done(err, null);
    }
});

route.get('/auth-login', (req,res)=>{
    
    const data = {
        "title":"Login",
        "message":"Login"
    }

    res.render('login', data)
})
route.get('/auth-logout', (req,res)=>{
    req.logout((err) => {
        if (err) {
            console.error('Error logging out:', err);
            return res.status(500).json({ message: 'Error logging out' });
        }
        req.session.destroy((err) => {
            if (err) {
                console.error('Error destroying session:', err);
                return res.status(500).json({ message: 'Error logging out' });
            }
            res.status(200).json({ message: 'Logout successful' });
        });
    });
    res.redirect('/app/auth-login')
})
route.get('/auth-register', (req,res)=>{

    const data = {
        "title":"Register",
        "message":"Registration"
    }

    res.render('register', data)

})

route.post('/login', passport.authenticate('local'), (req, res) => {
    res.json({ message: 'Login successful', user: req.user });
    return res.redirect('/app/home')
});

route.post('/logout', (req, res)=>{
    req.logout((err) => {
        if (err) {
            console.error('Error logging out:', err);
            return res.status(500).json({ message: 'Error logging out' });
        }
        req.session.destroy((err) => {
            if (err) {
                console.error('Error destroying session:', err);
                return res.status(500).json({ message: 'Error logging out' });
            }
            res.status(200).json({ message: 'Logout successful' });
        });
    });
    return res.redirect('app/auth-login')
})
route.post('/register', async(req, res)=>{
    const {name, surname,username, password } = req.body;
    const hashedPassword = await bcrypt.hash(password, 10);
    try{
        const existingUser = await User.findOne({ username });

        if (existingUser) {
            return res.status(400).json({ message: 'Username already exists' });
        }
        const newUser = new User({
                name:name,
                surname:surname,
                username:username,
                password : hashedPassword
        });
        await newUser.save();
        res.status(201).json({ message: 'User registered successfully' });
    }
    catch (error) {
        console.error('Error registering user:', error);
        res.status(500).json({ message: 'Internal server error' });
    }
})

route.get('/home', isAuthenticated, (req, res) => {
    const data = {
        "title":"Home",
        "user":req.user
    }

    res.render('home', data)
});

module.exports = route;